import pytest
from ..src.case_handlers import *

def get_asset_path( asset):
    tests_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(tests_path, "test_assets", asset)
    return file_path


def test_file_exists():
    asset_path = get_asset_path("file.html")
    file_exists_handler = CaseFileExistsHandler()
    assert file_exists_handler.test(asset_path) == True

def test_file_exists2():
    asset_path = get_asset_path("file.html")
    file_exists_handler = CaseFileExistsHandler()
    assert "This file exists" in file_exists_handler.run(asset_path)
    
